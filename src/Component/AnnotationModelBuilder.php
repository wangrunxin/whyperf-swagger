<?php

namespace WhyperfSwagger\Component;

use Hyperf\Validation\Request\FormRequest;
use PhpParser\Comment\Doc;
use WhyperfSwagger\Builder\SingleActionManager;
use WhyperfSwagger\Model\SwaggerAnnotationControllerManager;
use WhyperfSwagger\Tag\Parameter;
use WhyperfSwagger\Tag\Post;
use WhyperfSwagger\Tag\Response;

class AnnotationModelBuilder
{

    protected $request;

    protected $response;

    protected $actionManager;

    public AnnotationBuilder $builder;

    function __construct(SingleActionManager $actionManager)
    {
        $this->builder = make(AnnotationBuilder::class);
        $this->actionManager = $actionManager;
    }

    /**
     * @param FormRequest $request
     * @return $this
     */
    function setRequest(FormRequest $request): self
    {
        $this->request = $request;
        return $this;
    }

    function generate()
    {
        $this->createRequestAnnotation();
        $doc = new Doc($this->builder->generateDocBlock());
        return $doc;
    }

    protected function createRequestAnnotation()
    {
        if (!method_exists($this->request, "rules")) {
            return;
        }

        $post = new Post();
        $post->tags = $this->getTags();
        $post->path = $post->operationId = SwaggerAnnotationControllerManager::getPath($this->actionManager->getClass()->getName(), $this->actionManager->getAction());
        $response = new Response();
        $response->response = 200;
        $response->description = "success";
        $post->addObject($response);
        $this->builder->addObject($post);
        foreach ($this->request->rules() as $key => $rules) {
            $formdata = new Parameter([
                "name" => $key,
                "in" => "query"
            ]);
            $formdata->translateRules($rules);
            $post->addObject($formdata);
        }
    }

    protected function getTags(): array
    {
        $tags = [str_replace("Controller", "", $this->actionManager->getClass()->getShortName())];;
        $className = $this->actionManager->getClass()->getName();
        if (method_exists($className, "tags")) {
            $tags = array_merge($tags, $className::tags()[$this->actionManager->getAction()] ?? []);
        }
        return $tags;
    }
}