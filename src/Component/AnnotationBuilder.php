<?php

namespace WhyperfSwagger\Component;

use WhyperfSwagger\Tag\DocBuilderTrait;

class AnnotationBuilder
{
    use DocBuilderTrait;

    function generateDocBlock()
    {
        $lines = [];
        $lines[] = '/**';

        /**
         * @var $object $object
         */
        foreach ($this->objects as $object) {
            $reflect = new \ReflectionClass($object);
            $lines[] = sprintf(' * @OA\%s(%s)', $reflect->getShortName(), $this->objectToString($object));
        }
        $lines[] = ' */';
        return implode("\n", $lines);
    }


}
