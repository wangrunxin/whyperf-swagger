<?php

namespace WhyperfSwagger\Annotation;

use Hyperf\Apidog\Annotation\ApiDefinition;
use PhpParser\Comment\Doc;
use PhpParser\Node;
use PhpParser\Node\Stmt\Class_;

class RewriteClassVisitor extends AbstractNodeVisitor
{
    public function leaveNode(Node $node)
    {
        if ($node instanceof Class_) {
            $this->replaceClass($node);
        }
    }

    protected function replaceClass(Class_ $node)
    {
        $className = $this->classManager->getClass()->getName();
        $description = $tag = str_replace("Controller", "", $this->classManager->getClass()->getShortName());

        if (method_exists($className, "getDescription")) {
            $description = $className::getDescription();
        }

        $node->implements = [];
        $node->name->name .= "Annotation";
        $node->setDocComment(new Doc(<<<DOC
/**
 * @SwaggerAnnotationController()
 * @OA\Tag(name="$tag", description="$description")
 */
DOC
        ));
        return $node;
    }
}


