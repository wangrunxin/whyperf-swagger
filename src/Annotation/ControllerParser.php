<?php

namespace WhyperfSwagger\Annotation;

use Exception;
use Hyperf\Contract\StdoutLoggerInterface;
use PhpParser\Node\Stmt;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter\Standard;
use Roave\BetterReflection\Reflection\ReflectionClass;
use Whyperf\Whyperf;
use WhyperfSwagger\Builder\Analyser;
use WhyperfSwagger\Builder\SingleClassManager;

class ControllerParser
{

    protected $astParser;

    protected $classManager;

    protected $printer;

    protected $stmts;

    protected $annotationClassName;

    /**
     * @param ReflectionClass $class
     */
    function __construct(SingleClassManager $classManager)
    {
        $this->astParser = (new ParserFactory())->create(ParserFactory::ONLY_PHP7);
        $this->printer = new Standard();
        $this->classManager = $classManager;
        $this->annotationClassName = $this->classManager->getClass()->getShortName() . "Annotation";
        $this->process();
    }

    /**
     * Registry for the post-processing operations.
     *
     * @var array
     */
    protected static $visitors;

    public static function &visitors()
    {
        if (!self::$visitors) {
            // Add default processors.
            self::$visitors = [
            ];
        }

        return self::$visitors;
    }

    function handle()
    {
        if (count($this->classManager->getActions()) == 0) {
            return Whyperf::getContainer()->get(StdoutLoggerInterface::class)->debug(sprintf("%s has no valid action, skip", $this->classManager->getClass()->getName()));
        }

        $traverser = new NodeTraverser();
        foreach (self::$visitors as $visitorClass) {
            /**
             * @var NodeVisitorAbstract $visitor
             */
            $visitor = make($visitorClass, [$this->classManager]);
            $traverser->addVisitor($visitor);
        }
        $this->stmts = $traverser->traverse($this->stmts);
        $this->save();
    }

    protected function save()
    {
        $this->createOpenapiDir();
        $this->deleteExistedFile();

//        var_dump($this->printer->prettyPrintFile($this->stmts));
        file_put_contents($this->getFilePath(), $this->printer->prettyPrintFile($this->stmts));
    }

    protected function createOpenapiDir()
    {
        $dir = Whyperf::getRuntimePath() .  DIRECTORY_SEPARATOR . "openapi";
        if (!file_exists($dir)) {
            mkdir($dir);
        }
    }

    /**
     * @throws Exception
     */
    function createInfoFile()
    {
        $file = Whyperf::getRuntimePath() . "openapi" . DIRECTORY_SEPARATOR . "Info.php";
        $template = __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "Info.php";
        if (file_exists($file)) {
            unlink($file);
        }
        if (!file_exists($template)) {
            throw new Exception("Template does not exist");
        }

        $this->stmts = $this->astParser->parse(file_get_contents($this->classManager->getClass()->getFileName()));
    }

    protected function getFilePath()
    {
        return Whyperf::getRuntimePath() . DIRECTORY_SEPARATOR . "openapi" . DIRECTORY_SEPARATOR . $this->annotationClassName . ".php";
    }

    protected function deleteExistedFile()
    {
        $file = $this->getFilePath();
        if (file_exists($file)) {
            unlink($file);
        }
    }


    /**
     * Register an action.
     *
     * @param array $processors
     *
     */
    public static function registerVisitors(array $visitors): void
    {
        foreach ($visitors as $visitor) {
            array_push(self::visitors(), $visitor);
        }
    }


    protected function process()
    {
        $this->stmts = $this->astParser->parse(file_get_contents($this->classManager->getClass()->getFileName()));
    }

    /**
     * @return array
     */
    function getStmts(): array
    {
        return $this->stmts;
    }

    /**
     * @param array $stmts
     * @return $this
     */
    function setStmts(array $stmts): self
    {
        $this->stmts = $stmts;
        return $this;
    }
}
