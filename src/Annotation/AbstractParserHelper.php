<?php

namespace WhyperfSwagger\Annotation;

abstract class AbstractParserHelper{

    protected $parser;

    function __construct(ControllerParser $parser)
    {
        $this->parser = $parser;
    }

    abstract function handle();
}
