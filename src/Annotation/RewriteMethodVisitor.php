<?php

namespace WhyperfSwagger\Annotation;

use PhpParser\Comment\Doc;
use PhpParser\Node;
use PhpParser\Node\Stmt\Class_;
use PhpParser\NodeTraverser;
use PhpParser\Node\Stmt\ClassMethod;

class RewriteMethodVisitor extends AbstractNodeVisitor
{

    public function leaveNode(Node $node)
    {
        if ($node instanceof ClassMethod) {
            return $this->handleClassMethod($node);
        }
    }

    protected function handleClassMethod(ClassMethod $node)
    {
        if (!$this->isAnActionMethod($node)) {
            return NodeTraverser::REMOVE_NODE;
        }

        return $this->handleActionNode($node);
    }

    protected function isAnActionMethod(ClassMethod $node): bool
    {
        if (isset($this->classManager->getActions()[$node->name->name])) {
            return true;
        }
        return false;
    }

    protected function handleActionNode(ClassMethod $node): ClassMethod
    {
        $actionManager = $this->classManager->getActions()[$node->name->name];
        $node->setDocComment($actionManager->getDoc());
        return $this->removeLogicCode($node);
    }

    protected function removeLogicCode(ClassMethod $node): ClassMethod
    {
        $node->stmts = [];
        return $node;
    }
}