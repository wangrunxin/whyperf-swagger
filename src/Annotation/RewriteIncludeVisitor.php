<?php

namespace WhyperfSwagger\Annotation;

use OA\Info;
use OpenApi\Annotations\Parameter;
use OpenApi\Annotations\Post;
use PhpParser\Comment\Doc;
use PhpParser\Node;
use PhpParser\Node\Stmt\Class_;
use WhyperfSwagger\Model\SwaggerAnnotationController;

class RewriteIncludeVisitor extends AbstractNodeVisitor
{
    public function leaveNode(Node $node)
    {
        if ($node instanceof Class_) {
            return $this->inlcudeAnnotationClass($node);
        }
    }

    protected function inlcudeAnnotationClass(Class_ $node)
    {
        $nodes = [];
        $annotations = [
            SwaggerAnnotationController::class,
            'WhyperfSwagger\Tag as OA'
        ];

        foreach ($annotations as $class) {
            array_push($nodes, (
            new Node\Stmt\UseUse(new Node\Name($class)))
            );
        }
        $use = new Node\Stmt\Use_($nodes);
        return [
            $use, $node
        ];
    }
}


