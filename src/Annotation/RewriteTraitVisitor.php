<?php


namespace WhyperfSwagger\Annotation;


use PhpParser\Node;
use PhpParser\Node\Stmt\TraitUse;
use PhpParser\NodeTraverser;

class RewriteTraitVisitor extends AbstractNodeVisitor
{

    public function leaveNode(Node $node)
    {
        if ($node instanceof TraitUse) {
            return NodeTraverser::REMOVE_NODE;
        }
    }
}