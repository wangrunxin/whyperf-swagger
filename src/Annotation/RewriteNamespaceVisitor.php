<?php

namespace WhyperfSwagger\Annotation;

use PhpParser\Node;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\NodeVisitorAbstract;

class RewriteNamespaceVisitor extends AbstractNodeVisitor
{

    public function leaveNode(Node $node)
    {
        if ($node instanceof Namespace_) {
            $this->replaceNamespace($node);
        }
    }

    protected function replaceNamespace(Namespace_ $node)
    {
        $node->name->parts = [
            'Runtime',
            'openapi'
        ];
        return $node;
    }
}


