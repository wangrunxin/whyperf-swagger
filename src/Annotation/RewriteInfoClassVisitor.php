<?php

namespace WhyperfSwagger\Annotation;

use Hyperf\Apidog\Annotation\ApiDefinition;
use Hyperf\Contract\ConfigInterface;
use Hyperf\Utils\ApplicationContext;
use PhpParser\Comment\Doc;
use PhpParser\Node;
use PhpParser\Node\Stmt\Class_;
use WhyperfSwagger\Tag\Info;

class RewriteInfoClassVisitor extends AbstractNodeVisitor
{
    /**
     * @var Info
     */
    protected $info;

    public function leaveNode(Node $node)
    {
        if ($node instanceof Class_) {
            $this->replaceClass($node);
        }
    }

    protected function replaceClass(Class_ $node)
    {
        $this->buildInfo();
        $doc = $this->info->handObject($this->info);
        $node->setDocComment(new Doc(<<<DOC
/**
 * @SwaggerAnnotationController()
$doc
 */
DOC
        ));
        return $node;
    }

    protected function buildInfo()
    {
        $config = ApplicationContext::getContainer()->get(ConfigInterface::class);
        $data = $config->get("swagger.swagger");

        $this->info = new Info();
        $this->info->title = $data["info"]["title"] ?? "hyperf-api";
        $this->info->description = $data["info"]["description"] ?? "hyperf-api doc";
        $this->info->version = $data["info"]["version"] ?? "1.0.0";
    }
}


