<?php

namespace WhyperfSwagger\Annotation;

use Hyperf\Di\Annotation\AnnotationCollector;
use PhpParser\NodeVisitorAbstract;
use WhyperfSwagger\Builder\SingleClassManager;

class AbstractNodeVisitor extends NodeVisitorAbstract
{
    /**
     * @var SingleClassManager
     */
    protected $classManager;

    function __construct(?SingleClassManager $classManager)
    {
        $this->classManager = $classManager;
    }

}