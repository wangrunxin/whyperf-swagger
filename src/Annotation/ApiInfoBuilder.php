<?php

namespace WhyperfSwagger\Annotation;

use Exception;
use Hyperf\Contract\StdoutLoggerInterface;
use PhpParser\Node\Stmt;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitorAbstract;
use PhpParser\ParserFactory;
use PhpParser\PrettyPrinter\Standard;
use Roave\BetterReflection\Reflection\ReflectionClass;
use Whyperf\Whyperf;
use WhyperfSwagger\Builder\Analyser;
use WhyperfSwagger\Builder\SingleClassManager;

class ApiInfoBuilder
{

    protected $astParser;

    protected $printer;

    protected $stmts;

    protected $annotationClassName;

    /**
     * Registry for the post-processing operations.
     *
     * @var array
     */
    protected static $visitors;

    /**
     * @param ReflectionClass $class
     */
    function __construct()
    {
        $this->astParser = (new ParserFactory())->create(ParserFactory::ONLY_PHP7);
        $this->printer = new Standard();
        $this->annotationClassName = "ApiInfo";
        $this->loadTemplate();
    }

    public static function &visitors(): array
    {
        if (!self::$visitors) {
            // Add default processors.
            self::$visitors = [
                RewriteNamespaceVisitor::class,
                RewriteInfoClassVisitor::class
            ];
        }

        return self::$visitors;
    }

    function handle()
    {
        $traverser = new NodeTraverser();
        foreach (self::visitors() as $visitorClass) {
            /**
             * @var NodeVisitorAbstract $visitor
             */
            $visitor = make($visitorClass, [null]);
            $traverser->addVisitor($visitor);
        }
        $this->stmts = $traverser->traverse($this->stmts);
        $this->save();
    }

    protected function save()
    {
        $this->createOpenapiDir();
        $this->deleteExistedFile();
        file_put_contents($this->getFilePath(), $this->printer->prettyPrintFile($this->stmts));
    }

    protected function createOpenapiDir()
    {
        $dir = Whyperf::getRuntimePath() . DIRECTORY_SEPARATOR . "openapi";
        if (!file_exists($dir)) {
            mkdir($dir);
        }
    }

    protected function getTemplateFile(): string
    {
        return __DIR__ . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "Model" . DIRECTORY_SEPARATOR . "ApiInfo.php";
    }

    /**
     * @throws Exception
     */
    function deleteExistedFile()
    {
        $file = $this->getFilePath();
        if (file_exists($file)) {
            unlink($file);
        }
    }

    protected function getFilePath()
    {
        return Whyperf::getRuntimePath() . DIRECTORY_SEPARATOR . "openapi" . DIRECTORY_SEPARATOR . $this->annotationClassName . ".php";
    }


    /**
     * Register a visitor.
     *
     * @param array $visitors
     */
    public static function registerVisitors(array $visitors): void
    {
        foreach ($visitors as $visitor) {
            self::visitors()[] = $visitor;
        }
    }


    protected function loadTemplate()
    {
        $template = $this->getTemplateFile();
        if (!file_exists($template)) {
            throw new Exception("Template does not exist");
        }
        $this->stmts = $this->astParser->parse(file_get_contents($template));
    }

    /**
     * @return array
     */
    function getStmts(): array
    {
        return $this->stmts;
    }

    /**
     * @param array $stmts
     * @return $this
     */
    function setStmts(array $stmts): self
    {
        $this->stmts = $stmts;
        return $this;
    }
}
