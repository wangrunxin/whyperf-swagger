<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */

namespace WhyperfSwagger;

use Hyperf\Contract\ConfigInterface;
use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\ReflectionManager;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Framework\Event\BootApplication;
use Hyperf\Utils\ApplicationContext;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Whyperf\Whyperf;
use WhyperfSwagger\Command\GenCommand;
use function Composer\Autoload\includeFile;

class BootApplicationListener implements ListenerInterface
{
    protected $config;

    public function listen(): array
    {
        return [
            BootApplication::class,
        ];
    }

    public function process(object $event)
    {
        $this->config = ApplicationContext::getContainer()->get(ConfigInterface::class);
        if (!$this->isEnable()) {
            return;
        }
        $paths = [
            Whyperf::getRuntimePath() . DIRECTORY_SEPARATOR . "openapi"
        ];
        $this->clearComponents();
        $command = new GenCommand();
        $command->handle();
        $this->clearCache($paths);
        $this->buildSwagger($paths);
    }

    protected function isEnable(): bool
    {
        if (!$this->config->get('swagger.enable')) {
            $io = new SymfonyStyle(new ArrayInput([]), new ConsoleOutput());
            $io->note("auto-build swagger.json is disabled, config at: confg->swagger->enable.");
            return false;
        }
        return true;
    }

    protected function clearCache($paths)
    {
        $finder = new Finder();
        $finder->files()->in($paths)->name('*.php');
        foreach ($finder as $file) {
            try {
                includeFile($file->getPathname());
            } catch (\Throwable $e) {
            }
        }
    }

    protected function clearComponents()
    {
        $path = config("server.settings.document_root");
        if (is_null($path)) {
            return;
        }
        $path .= "/components";
        $finder = new Finder();
        if (!file_exists($path)) {
            return;
        }
        $finder->files()->in([$path])->name('*.json');
        foreach ($finder as $file) {
            try {
                unlink($file->getPathname());
            } catch (\Throwable $e) {
            }
        }
    }

    public function buildSwagger($paths)
    {
        $classes = ReflectionManager::getAllClasses($paths);

        foreach ($classes as $className => $reflectionClass) {
            AnnotationCollector::clear($className);
        }
        $openapi = \OpenApi\Generator::scan($paths);
        file_put_contents(BASE_PATH . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . "swagger.json", $openapi->toJson());
    }

    protected function prepareHandler($handler): array
    {
        if (is_string($handler)) {
            if (strpos($handler, '@') !== false) {
                return explode('@', $handler);
            }
            return explode('::', $handler);
        }
        if (is_array($handler) && isset($handler[0], $handler[1])) {
            return $handler;
        }
        throw new \RuntimeException('Handler not exist.');
    }
}
