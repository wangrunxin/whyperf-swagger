<?php

namespace WhyperfSwagger\Builder;

use Hyperf\Apidog\Annotation\FormData;
use Hyperf\Apidog\Annotation\PostApi;
use Hyperf\Di\MethodDefinitionCollector;
use Hyperf\Di\ReflectionType;
use Hyperf\Validation\Request\FormRequest;
use PhpParser\Comment\Doc;
use PHPStan\BetterReflection\Reflection\ReflectionClass;
use Whyperf\Validator\ValidatedRequest;
use Whyperf\Whyperf;
use WhyperfSwagger\Annotation\AnnotationBuilder;
use WhyperfSwagger\Component\AnnotationModelBuilder;
use WhyperfSwagger\Model\SwaggerAnnotationControllerManager;
use WhyperfSwagger\OpenApi\OAParameter;
use WhyperfSwagger\OpenApi\OAPost;

class SingleActionManager
{
    protected $methodDefinitioinController;
    protected $classManager;
    protected $validatorClass;
    protected $action;
    protected $requestDoc;

    function __construct(SingleClassManager $classManager, string $action)
    {
        $this->methodDefinitioinController = new MethodDefinitionCollector();
        $this->classManager = $classManager;
        $this->action = $action;
    }

    function getClass(): ReflectionClass
    {
        return $this->classManager->getClass();
    }

    function getAction(): string
    {
        return $this->action;
    }

    protected function getMethodDefinitionCollector(): MethodDefinitionCollector
    {
        return $this->methodDefinitioinController;
    }

    function process()
    {
        $definitions = $this->getMethodDefinitionCollector()->getParameters($this->classManager->getClass()->getName(), $this->action);
        return $this->getInjections($definitions);
    }

    protected function getInjections($definitions)
    {
        /**
         * @var ReflectionType $reflectionType
         */
        foreach ($definitions as $reflectionType) {
            $validatorClass = $reflectionType->getName();
            if (is_subclass_of($validatorClass, FormRequest::class)) {
                $this->validatorClass = $validatorClass;
                $this->classManager->registerAction($this);
            }
        }
    }

    protected function getRequestFormValidator($validatorClass): FormRequest
    {
        return new $validatorClass(Whyperf::getContainer());
    }

    function getDoc(): Doc
    {
        $formRequest = $this->getRequestFormValidator($this->validatorClass);
        /**
         * @var AnnotationModelBuilder $builder
         */
        $builder = make(AnnotationModelBuilder::class, [$this]);
        return $builder->setRequest($formRequest)->generate();
    }
}