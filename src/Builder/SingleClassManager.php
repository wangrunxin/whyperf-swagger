<?php

namespace WhyperfSwagger\Builder;


use PHPStan\BetterReflection\Reflection\ReflectionClass;

class SingleClassManager
{

    /**
     *
     * @var SingleActionManager[]
     */
    protected $actions = [];

    /**
     * @var ReflectionClass
     */
    protected $class;

    function __construct(ReflectionClass $class, $metaData)
    {
        $this->class = $class;
    }

    function getClass(): ReflectionClass
    {
        return $this->class;
    }

    function getActions(): array
    {
        return $this->actions;
    }

    /**
     * Register an action.
     *
     * @param SingleActionManager $action
     *
     */
    function registerAction(SingleActionManager $action): void
    {
        $this->actions[$action->getAction()] = $action;
    }

    function processTrait()
    {
        return;
        foreach ($this->class->getTraits() as $trait) {
        }
    }

    function process()
    {
        foreach ($this->class->getMethods() as $method) {
            $actionManager = new SingleActionManager($this, $method->getName());
            $actionManager->process();
        }

        $this->processTrait();
    }

}