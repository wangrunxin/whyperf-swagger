<?php

namespace WhyperfSwagger\Builder;

use Hyperf\Contract\StdoutLoggerInterface;
use Hyperf\Di\Annotation\AnnotationCollector;
use OpenApi\StaticAnalyser;
use OpenApi\Util;
use PHPStan\BetterReflection\BetterReflection;
use PHPStan\BetterReflection\Reflector\ClassReflector;
use PHPStan\BetterReflection\SourceLocator\Type\SingleFileSourceLocator;
use Symfony\Component\Finder\Exception\DirectoryNotFoundException;
use Symfony\Component\Finder\Finder;
use Whyperf\Whyperf;
use WhyperfSwagger\Annotation\ApiInfoBuilder;
use WhyperfSwagger\Annotation\ControllerParser;
use WhyperfSwagger\Annotation\NamespaceBuilder;
use WhyperfSwagger\Annotation\RewriteClassVisitor;
use WhyperfSwagger\Annotation\RewriteIncludeVisitor;
use WhyperfSwagger\Annotation\RewriteMethodVisitor;
use WhyperfSwagger\Annotation\RewriteNamespaceVisitor;
use WhyperfSwagger\Annotation\RewriteTraitVisitor;
use WhyperfSwagger\Model\SwaggerAnnotationControllerManager;

class SwaggerManager
{
    protected string $path;

    protected $analyser;

    protected $collector;

    function __construct($path)
    {
        $this->path = BASE_PATH . '/' . $path;
        $this->collector = AnnotationCollector::list();
        SwaggerAnnotationControllerManager::getInstance()->initSwaggerAnnotationRoute($this->collector);
    }

    function generate()
    {
        try {
            $this->registerProcessor();
            $this->scanSources($this->scan());
            $info = new ApiInfoBuilder();
            $info->handle();;
        } catch (DirectoryNotFoundException $e) {
            Whyperf::getContainer()->get(StdoutLoggerInterface::class)->debug(sprintf("Auto-build swagger.json failed: [%s]", $e->getMessage()));
        }
    }

    function getPath()
    {
        return $this->path;
    }

    /**
     * @param $options
     * @return \Symfony\Component\Finder\Finder
     */
    protected function scan($options = []): Finder
    {
        $exclude = array_key_exists('exclude', $options) ? $options['exclude'] : null;
        $pattern = array_key_exists('pattern', $options) ? $options['pattern'] : null;

        return Util::finder($this->path, $exclude, $pattern);
    }

    public function getAnalyser(): Analyser
    {
        return $this->analyser ?: Whyperf::getContainer()->get(Analyser::class);
    }

    protected function scanSources(iterable $sources): void
    {
        foreach ($sources as $source) {
            if (is_iterable($source)) {
                $this->scanSources($source);
            } else {
                $resolvedSource = $source instanceof \SplFileInfo ? $source->getPathname() : realpath($source);
                if (!$resolvedSource) {
                    Whyperf::getContainer()->get(StdoutLoggerInterface::class)->debug(sprintf('Invalid source: %s - [missing], skip', $source));
                    continue;
                }
                if (is_dir($resolvedSource)) {
                    $this->scanSources(Util::finder($resolvedSource));
                } else {
                    $astLocator = (new BetterReflection())->astLocator();
                    $reflector = new ClassReflector(new SingleFileSourceLocator($resolvedSource, $astLocator));
                    foreach ($reflector->getAllClasses() as $object) {
                        if (!isset($this->collector[$object->getName()])) {
                            Whyperf::getContainer()->get(StdoutLoggerInterface::class)->debug(sprintf('Invalid class: %s - [no annotation], skip', $object->getName()));
                            continue;
                        }
                        $classManager = new SingleClassManager($object, $this->collector[$object->getName()]);
                        $classManager->process();
                        $builder = new ControllerParser($classManager);
                        $builder->handle();
                    }
                }
            }
        }
    }

    protected function registerProcessor()
    {
        ControllerParser::registerVisitors([
            RewriteNamespaceVisitor::class,
            RewriteClassVisitor::class,
            RewriteMethodVisitor::class,
            RewriteIncludeVisitor::class,
            RewriteTraitVisitor::class,
        ]);
    }
}