<?php

namespace WhyperfSwagger\Tag;


/**
 * @Annotation
 */
class License extends \OpenApi\Annotations\License {
    use AnnotationTrait;
    use DocBuilderTrait;
}