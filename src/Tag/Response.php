<?php

namespace WhyperfSwagger\Tag;

/**
 * @Annotation
 */
class Response extends \OpenApi\Annotations\Response {
    use AnnotationTrait;
    use DocBuilderTrait;
}