<?php

namespace WhyperfSwagger\Tag;


use OpenApi\Generator;
use WhyperfSwagger\Validation\RuleTranslator;

/**
 * @Annotation
 */
class Parameter extends \OpenApi\Annotations\Parameter
{
    use AnnotationTrait;
    use DocBuilderTrait;

    function translateRules($rules)
    {
        if (is_string($rules)) {
            $rules = explode("|", $rules);
        }
        if (is_array($rules)) {
            foreach ($rules as $rule) {
                /**
                 * @var RuleTranslator $translator
                 */
                $translator = make(RuleTranslator::class, [$rule]);
                if ($translator->catch()) {
                    $translator->translate($this);
                }
            }
        }
    }

    function getSchema(): Schema
    {
        if (empty($this->schema) || $this->schema == Generator::UNDEFINED) {
            $this->schema = new Schema();
        }

        return $this->schema;
    }
}