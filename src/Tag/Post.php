<?php

namespace WhyperfSwagger\Tag;

/**
 * @Annotation
 */
class Post extends \OpenApi\Annotations\Post {
    use AnnotationTrait;
    use DocBuilderTrait;
}