<?php

namespace WhyperfSwagger\Tag;

use OpenApi\Annotations\AbstractAnnotation;
use OpenApi\Annotations\OpenApi;
use OpenApi\Context;
use OpenApi\Generator;

trait AnnotationTrait
{
    public function __construct(array $properties = [])
    {
        foreach ($properties as $property => $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
                if (is_array($value)) {
                    foreach ($value as $key => $annotation) {
                        if (is_object($annotation) && $annotation instanceof AbstractAnnotation) {
                            $this->$property[$key] = $this->nested($annotation, $nestedContext);
                        }
                    }
                }
            } elseif ($property !== 'value') {
                $this->$property = $value;
            } elseif (is_array($value)) {
                $annotations = [];
                foreach ($value as $annotation) {
                    if ($annotation instanceof AbstractAnnotation) {
                        $annotations[] = $annotation;
                    } else {
                        $this->_context->logger->warning('Unexpected field in ' . $this->identity() . ' in ' . $this->_context);
                    }
                }
                $this->merge($annotations);
            } elseif (is_object($value)) {
                $this->merge([$value]);
            } else {
                if ($value !== Generator::UNDEFINED) {
                    $this->_context->logger->warning('Unexpected parameter "' . $property . '" in ' . $this->identity());
                }
            }
        }
    }
}