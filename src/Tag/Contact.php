<?php

namespace WhyperfSwagger\Tag;


/**
 * @Annotation
 */
class Contact extends \OpenApi\Annotations\Contact{
    use AnnotationTrait;
    use DocBuilderTrait;
}