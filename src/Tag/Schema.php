<?php

namespace WhyperfSwagger\Tag;

/**
 * @Annotation
 */
class Schema extends \OpenApi\Annotations\Schema {
    use AnnotationTrait;
    use DocBuilderTrait;
}