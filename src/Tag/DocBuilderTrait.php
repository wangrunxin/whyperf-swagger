<?php

namespace WhyperfSwagger\Tag;

use OpenApi\Annotations\AbstractAnnotation;
use OpenApi\Annotations\OpenApi;
use OpenApi\Context;
use OpenApi\Generator;

trait DocBuilderTrait
{
    protected array $objects = [];

    function addObject($object): self
    {
        $this->objects[] = $object;
        return $this;
    }

    function hasObject()
    {
        return count($this->objects) != 0;
    }

    function handObject($object): string
    {
        $reflect = new \ReflectionClass($object);
        return sprintf(<<<DOC
 * @OA\%s(
%s
 * )
DOC, $reflect->getShortName(), $this->objectToString($object));
    }

    function generateDocBlock(): string
    {
        $lines = [];

        /**
         * @var $object $object
         */
        foreach ($this->objects as $object) {
            $lines[] = $this->handObject($object);
        }
        return implode(",\n", $lines);
    }

    function objectToString(object $object): string
    {
        $doc = $this->getObjectVars($object);

        if (self::hasTrait($object, DocBuilderTrait::class)) {
            /**
             * @var DocBuilderTrait $object
             */
            if ($object->hasObject()) {
                return implode(",\n", [$doc, $object->generateDocBlock()]);
            }
        }
        return $doc;
    }

    function getObjectVars($object): string
    {
        $input = get_object_vars($object);
        foreach ($input as $key => $val) {
            if ($val === Generator::UNDEFINED || empty($val)) {
                unset($input[$key]);
            }
        }
        return implode(",\n", array_map(
            function ($v, $k) {
                return $this->convertKeyValue($k, $v);
            },
            $input,
            array_keys($input)
        ));
    }

    function convertKeyValue($k, $v): string
    {

        if (is_array($v)) {
            return sprintf(' * %s={%s}', $k, $this->arrayToString($v));
        }

        if (is_object($v)) {
            if($v instanceof Schema || is_subclass_of($v, Schema::class)){
                $result =  $this->handObject($v);
                return $result;
            }

            return $this->objectToString($v);
        }

        if (is_bool($v)) {
            return sprintf(' * %s=%s', $k, $v ? "true" : "false");
        }

        return sprintf(' * %s="%s"', $k, $v);
    }

    function arrayToString(array $input): string
    {
        return implode(",\n", array_map(
            function ($v, $k) {
                return $this->convertKeyValue($k, $v);
            },
            $input,
            array_keys($input)
        ));
    }

    static function hasTrait($object, $traitName, $autoloader = true)
    {
        $ret = class_uses($object, $autoloader);
        if (is_array($ret)) {
            $ret = array_search($traitName, $ret) !== false;
        }
        return $ret;
    }
}