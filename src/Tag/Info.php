<?php

namespace WhyperfSwagger\Tag;


/**
 * @Annotation
 */
class Info extends \OpenApi\Annotations\Info{
    use AnnotationTrait;
    use DocBuilderTrait;
}