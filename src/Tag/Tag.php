<?php

namespace WhyperfSwagger\Tag;

/**
 * @Annotation
 */
class Tag extends \OpenApi\Annotations\Tag {
    use AnnotationTrait;
    use DocBuilderTrait;
}