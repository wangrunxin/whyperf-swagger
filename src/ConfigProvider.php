<?php

declare(strict_types=1);

namespace WhyperfSwagger;

use WhyperfSwagger\Command\GenCommand;

class ConfigProvider
{
    public function __invoke(): array
    {
        return [
            'commands' => [
                GenCommand::class,
            ],
            'dependencies' => [
                \Hyperf\HttpServer\Router\DispatcherFactory::class => SwaggerDispatcherFactory::class
            ],
            'listeners' => [
                BootApplicationListener::class,
            ],
            'annotations' => [
                'scan' => [
                    'paths' => [
                        __DIR__ . DIRECTORY_SEPARATOR . "Aspect",
                    ],
                ],
            ],
            'publish' => [
                [
                    'id' => 'config',
                    'description' => 'swagger config file',
                    'source' => __DIR__ . '/../publish/config.php',
                    'destination' => BASE_PATH . '/config/autoload/swagger.php',
                ],
                [
                    'id' => 'swagger_ui',
                    'description' => 'swagger html',
                    'source' => __DIR__ . '/../publish/swagger',
                    'destination' => BASE_PATH . '/public/swagger',
                ],
                [
                    'id' => 'redoc',
                    'description' => 'redoc html',
                    'source' => __DIR__ . '/../publish/redoc',
                    'destination' => BASE_PATH . '/public/redoc',
                ]
            ],
        ];
    }
}
