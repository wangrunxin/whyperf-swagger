<?php

declare(strict_types=1);

namespace WhyperfSwagger\Command;

use Hyperf\Command\Command;
use Hyperf\Contract\StdoutLoggerInterface;
use OpenApi\Analysis;
use Symfony\Component\Console\Input\InputOption;
use Whyperf\Whyperf;
use WhyperfSwagger\Builder\SwaggerManager;
use function OpenApi\scan;
use const OpenApi\UNDEFINED;

class GenCommand extends Command
{
    protected $name = 'swagger:create';

    public function handle()
    {
        $path = 'app/Controller';

        if (!is_null($this->input)) {
            $path = $this->input->getOption('path');
        }
        Whyperf::getContainer()->get(StdoutLoggerInterface::class)->info(sprintf("Swagger scan path: [%s]", $path));
        $manager = new SwaggerManager($path);
        $manager->generate();

        return $manager->getPath();
    }

    protected function getOptions(): array
    {
        return [
            ['path', 'p', InputOption::VALUE_OPTIONAL, 'The path that needs scan.', 'app/Controller'],
        ];
    }
}
