<?php

namespace WhyperfSwagger\Validation;

use Carbon\Doctrine\DateTimeType;
use WhyperfSwagger\Tag\Parameter;
use WhyperfSwagger\Tag\Schema;

class TypeRule extends AbstractRuleTranslator
{
    const TYPE_STRING = "string";
    const TYPE_INT = "int";
    const TYPE_DATETIME = "datetime";

    const FORMAT_INT32 = "int32";
    const FORMAT_DATETIME = "date-time";

    protected $data;
    protected $format;
    protected $type;

    function catch(): bool
    {
        if (!$this->isStringRule()) {
            return false;
        }

        if (in_array($this->rule, $this->getTypes())) {
            $this->data = $this->rule;
            return true;
        }

        if (strpos($this->rule, "datetime") === 0) {
            $this->data = self::TYPE_DATETIME;
            return true;
        }

        return false;
    }

    protected function getType()
    {
        if (in_array($this->data, [
            self::TYPE_STRING,
            self::TYPE_DATETIME
        ])) {
            return self::TYPE_STRING;
        }

        return $this->data;
    }

    protected function getFormat()
    {
        switch ($this->data) {
            case self::TYPE_DATETIME:
                return self::FORMAT_DATETIME;
            case self::TYPE_INT:
                return self::FORMAT_INT32;
            default:
                return null;
        }
    }

    protected function getTypes()
    {
        return [
            self::TYPE_INT,
            self::TYPE_STRING,
        ];
    }

    function translate(Parameter &$parameter)
    {
        $parameter->getSchema()->format = $this->getFormat();
        $parameter->getSchema()->type = $this->getType();
    }
}