<?php

namespace WhyperfSwagger\Validation;

use WhyperfSwagger\Tag\Parameter;

class BetweenRule extends AbstractRuleTranslator
{
    protected $min;

    protected $max;

    function catch(): bool
    {
        if (!$this->isStringRule()) {
            return false;
        }

        if (strpos($this->rule, "between:") !== 0) {
            return false;
        }

        $config = explode(",", str_replace("between:", "", $this->rule));
        if (count($config) != 2) {
            return false;
        }

        $this->min = $config[0] ?? null;
        $this->max = $config[1] ?? null;

        return is_numeric($this->min) && is_numeric($this->max);
    }

    function translate(Parameter &$parameter)
    {

        $parameter->getSchema()->minimum = $this->min;
        $parameter->getSchema()->maximum = $this->max;
    }
}