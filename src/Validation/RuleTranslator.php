<?php

namespace WhyperfSwagger\Validation;

use Hyperf\Contract\ConfigInterface;
use Hyperf\Utils\ApplicationContext;
use WhyperfSwagger\Tag\Parameter;

class RuleTranslator extends AbstractRuleTranslator
{
    /**
     * @var AbstractRuleTranslator
     */
    protected $targetRule;

    protected function getRules()
    {
        return ApplicationContext::getContainer()->get(ConfigInterface::class)->get("swagger.rules") ?? [
                TypeRule::class,
                RequiredRule::class,
            ];
    }

    function catch(): bool
    {
        foreach ($this->getRules() as $ruleClass) {
            /**
             * @var AbstractRuleTranslator $translator
             */
            $translator = make($ruleClass, [$this->rule]);
            if ($translator->catch()) {
                $this->targetRule = $translator;
                return true;
            }
        }

        return false;
    }

    function translate(Parameter &$parameter)
    {
        $this->targetRule->translate($parameter);
    }
}