<?php

namespace WhyperfSwagger\Validation;

use WhyperfSwagger\OpenApi\OAParameter;
use WhyperfSwagger\Tag\Parameter;

abstract class AbstractRuleTranslator
{
    protected $rule;

    function __construct($rule)
    {
        $this->rule = is_string($rule) ? strtolower($rule) : $rule;
    }

    protected function isStringRule(): bool
    {
        return is_string($this->rule);
    }

    abstract function catch(): bool;

    abstract function translate(Parameter &$parameter);
}