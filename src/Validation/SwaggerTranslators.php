<?php

namespace WhyperfSwagger\Validation;

use Whyperf\Model\Traits\Singleton;

class SwaggerTranslators
{

    use Singleton;

    function getTranslators(): array
    {
        return [
            TypeRule::class,
            BetweenRule::class,
            RequiredRule::class
        ];
    }

}