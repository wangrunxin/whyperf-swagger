<?php

namespace WhyperfSwagger\Validation;

use WhyperfSwagger\Tag\Parameter;

class RequiredRule extends AbstractRuleTranslator
{
    function catch(): bool
    {
        if ($this->isStringRule()) {
            return strtolower($this->rule) == "required";
        }
        return false;
    }

    function translate(Parameter &$parameter)
    {
        $parameter->required = true;
    }
}