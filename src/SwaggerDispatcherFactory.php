<?php

namespace WhyperfSwagger;

use Hyperf\Apidog\Annotation\ApiController;
use Hyperf\Apidog\Annotation\ApiVersion;
use Hyperf\Di\Annotation\AnnotationCollector;
use Hyperf\Di\Aop\AbstractAspect;
use Hyperf\Di\Aop\ProceedingJoinPoint;
use Hyperf\Di\Exception\ConflictAnnotationException;
use Hyperf\Di\ReflectionManager;
use Hyperf\HttpServer\Annotation\AutoController;
use Hyperf\HttpServer\Annotation\Mapping;
use Hyperf\HttpServer\Annotation\Middleware;
use Hyperf\HttpServer\Annotation\Middlewares;
use Hyperf\HttpServer\MiddlewareManager;
use Hyperf\Rpc\Contract\PathGeneratorInterface;
use Hyperf\RpcServer\Event\AfterPathRegister;
use Hyperf\RpcServer\Router\DispatcherFactory;
use Hyperf\Utils\Str;
use Psr\EventDispatcher\EventDispatcherInterface;
use WhyperfSwagger\Model\SwaggerAnnotationController;
use WhyperfSwagger\Model\SwaggerAnnotationControllerManager;

class SwaggerDispatcherFactory extends \Hyperf\HttpServer\Router\DispatcherFactory
{
    protected function initAnnotationRoute(array $collector): void
    {
        parent::initAnnotationRoute(SwaggerAnnotationControllerManager::getInstance()->initSwaggerAnnotationRoute($collector));
    }
}
