<?php

namespace WhyperfSwagger\Model;

use Hyperf\Di\Annotation\AbstractAnnotation;


/**
 * @Annotation
 * @Target({"CLASS"})
 */
#[Attribute(Attribute::TARGET_CLASS)]
class SwaggerAnnotationController extends AbstractAnnotation
{

}
