<?php

namespace WhyperfSwagger\Model;

use Hyperf\Apidog\Annotation\ApiController;
use Hyperf\Apidog\Annotation\ApiVersion;
use Hyperf\Di\ReflectionManager;
use Hyperf\HttpServer\Annotation\AutoController;
use Hyperf\HttpServer\Annotation\Mapping;
use Hyperf\Utils\Str;
use Whyperf\Model\Traits\Singleton;
use WhyperfSwagger\Builder\SingleActionManager;
use WhyperfSwagger\Model\SwaggerAnnotationController;
use ReflectionMethod;

class SwaggerAnnotationControllerManager
{

    use Singleton;

    /**
     *
     * @var array
     */
    protected static $vituralRoutes;

    /**
     *
     * @var array
     */
    protected static $swaggerRoutes;

    static function &vituralRoutes()
    {
        if (!self::$vituralRoutes) {
            // Add default processors.
            self::$vituralRoutes = [
            ];
        }

        return self::$vituralRoutes;
    }

    static function &swaggerRoutes()
    {
        if (!self::$swaggerRoutes) {
            // Add default processors.
            self::$swaggerRoutes = [
            ];
        }

        return self::$swaggerRoutes;
    }

    static function clear(){
        self::$vituralRoutes = [];
        self::$swaggerRoutes = [];
    }

    static function getPath($controller, $action)
    {
        if (!isset(self::vituralRoutes()[$controller][$action])) {
            return "unknown";
        }
        return self::vituralRoutes()[$controller][$action];
    }

    /**
     *
     * Register a vitural route.
     *
     * @param $className
     * @param $methodName
     * @param $path
     * @return void
     */
    protected function registerVituralRoutes($className, $methodName, $path): void
    {
        self::vituralRoutes()[$className][$methodName] = $path;
    }

    /**
     *
     * Register a vitural route.
     *
     * @param $className
     * @param $methodName
     * @param $path
     * @return void
     */
    protected function registerSwaggerRoutes($className, $methodName): void
    {
        self::swaggerRoutes()[$className][] = $methodName;
    }

    function initSwaggerAnnotationRoute(array $collector): array
    {
        foreach ($collector as $className => $metadata) {
            if (isset($metadata['_c'][AutoController::class])) {
                $this->handleController($className, $metadata['_c'][AutoController::class], $metadata['_m'] ?? []);
            }

            if (isset($metadata['_c'][ApiController::class])) {
                unset($collector[$className]);
            }
        }

        return $collector;
    }

    function initSwaggerDocRoutes(array $collector): array
    {
        foreach ($collector as $className => $metadata) {
            if (isset($metadata['_c'][ApiController::class])) {
                $this->handleSwaggerDocController($className, $metadata['_c'][ApiController::class], $metadata['_m'] ?? []);
            }
        }

        return $collector;
    }

    protected function getPrefix(string $className, string $prefix): string
    {
        if (!$prefix) {
            $handledNamespace = Str::replaceFirst('Controller', '', Str::after($className, '\\Controller\\'));
            $handledNamespace = str_replace('\\', '/', $handledNamespace);
            $prefix = Str::snake($handledNamespace);
            $prefix = str_replace('/_', '/', $prefix);
        }
        if ($prefix[0] !== '/') {
            $prefix = '/' . $prefix;
        }
        return $prefix;
    }

    protected function parsePath(string $prefix, ReflectionMethod $method): string
    {
        return $prefix . '/' . $method->getName();
    }

    protected function handleController(string $className, AutoController $annotation, array $methodMetadata): void
    {
        $class = ReflectionManager::reflectClass($className);
        $methods = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        $prefix = $this->getPrefix($className, $annotation->prefix);

        foreach ($methods as $method) {
            if ($method->getName() == "__construct") {
                continue;
            }
            $options = $annotation->options;
            $path = $this->parsePath($prefix, $method);
            $this->registerVituralRoutes($className, $method->getName(), $path);
        }
    }

    protected function handleSwaggerDocController(string $className, ApiController $annotation, array $methodMetadata): void
    {
        $class = ReflectionManager::reflectClass($className);
        $methods = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        $prefix = $this->getPrefix($className, $annotation->prefix);

        foreach ($methods as $method) {
            if ($method->getName() == "__construct") {
                continue;
            }
            $options = $annotation->options;
            $path = $this->parsePath($prefix, $method);
            $this->registerSwaggerRoutes($className, $method->getName());
        }
    }
}