<?php

namespace WhyperfSwagger\Model;

use WhyperfSwagger\Tag as OA;
use WhyperfSwagger\Model\SwaggerAnnotationController;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="openapi",
 *      description="hyperf-openapi",
 *      @OA\Contact(
 *          email="wangrunxin@wangrunxin.com"
 *      ),
 *     @OA\License(
 *         name="Apache 2.0",
 *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *     )
 * )
 */
class ApiInfo
{

}


?>