<?php

use WhyperfSwagger\Validation\RequiredRule;
use WhyperfSwagger\Validation\TypeRule;

return [
    'enable' => env('APP_ENV') !== 'production',
    'output_file' => BASE_PATH . '/public/swagger.json',
    'ignore' => function ($controller, $action) {
        return false;
    },
    'swagger' => [
        'swagger' => '3.0',
        'info' => [
            'description' => 'hyperf swagger api desc',
            'version' => '1.0.0',
            'title' => 'HYPERF API DOC',
            'contact' => [
                'email' => 'contact@example.com'
            ],
            'license' => [
                "name" => "Apache 2.0",
                "url" => "https://www.apache.org/licenses/LICENSE-2.0.html"
            ]
        ],
        'host' => 'localhost',
        'schemes' => ['http', 'https'],

    ],
    'rules' => make(\WhyperfSwagger\Validation\SwaggerTranslators::class)->getTranslators()
];
